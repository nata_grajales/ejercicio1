/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author ngrajales
 */
public class Ejercicio1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;

        try {
            archivo = new File(new File("").getAbsolutePath() + "/src/ejercicio1/personas.csv");
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);
            String linea;
            StringBuilder error = new StringBuilder();
            int numeroLinea = 0;
            while ((linea = br.readLine()) != null) {
                numeroLinea++;
                String[] line = linea.split(",");
                if (line.length != 3) {
                    error.append("Linea ").append(numeroLinea).append(" Incompleta \n");
                } else {
                    Persona p;
                    if (Integer.parseInt(line[2]) >= 18) {
                        if (line[1].equals("F")) {
                            p = new Mujer();
                            p.setNombre(line[0]);
                            p.setEdad(line[2]);
                            System.out.println(p.getSaludo());
                        } else {
                            p = new Hombre();
                            p.setNombre(line[0]);
                            p.setEdad(line[2]);
                            System.out.println(p.getSaludo());
                        }
                    } else if (Integer.parseInt(line[2]) < 18) {
                        if (line[1].equals("F")) {
                            p = new Nina();
                            p.setNombre(line[0]);
                            p.setEdad(line[2]);
                            System.out.println(p.getSaludo());
                        } else {
                            p = new Nino();
                            p.setNombre(line[0]);
                            p.setEdad(line[2]);
                            System.out.println(p.getSaludo());
                        }
                    }
                }

            }
        } catch (IOException e) {
        } finally {

            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (IOException e2) {
                System.out.println("e2 = " + e2);
            }
        }
    }

}
