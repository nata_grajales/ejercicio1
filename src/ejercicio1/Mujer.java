package ejercicio1;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ngrajales
 */
public class Mujer extends Persona {

    @Override
    public String getSaludo() {
        return new StringBuilder().append("Hola. Me llamo ").append(getNombre()).append(" y soy una ").append("Mujer").append(" de ").append(getEdad()).append(" años.").toString();
    }
}
