/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio1;

/**
 *
 * @author ngrajales
 */
public class Nina extends Persona {

    @Override
    public String getSaludo() {
        return new StringBuilder().append("Hola. Me llamo ").append(getNombre()).append(" y soy una ").append("Niña").append(" de ").append(getEdad()).append(" años.").toString();
    }
}
